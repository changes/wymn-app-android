package com.xiaoyun.org.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import cn.waps.AppConnect;

import com.xiaoyun.org.R;
import com.xiaoyun.org.app.common.Constant;
import com.xiaoyun.org.app.ui.MainActivity;
import com.xiaoyun.org.util.StringUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;


/**
 * 应用程序启动类：显示欢迎界面并跳转到主界面
 * 
 * @author yuanxy
 * @version v1.0
 */
public class AppStart extends Activity {

	private AppContext appContext;
	boolean isFirstIn = false;
	private static final int GO_HOME = 1000;
	private static final int GO_GUIDE = 1001;
	// 延迟1秒
	private static final long SPLASH_DELAY_MILLIS = 1000;
	private static final String SHAREDPREFERENCES_NAME = "first_pref";

	private String TAG = AppStart.class.getSimpleName();

	/**
	 * 
	 * Handler:跳转到不同界面
	 */

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case GO_HOME:
				goHome();
				break;
			case GO_GUIDE:
				goGuide();
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_start);
		appContext = (AppContext) getApplication();
		AppConnect.getInstance(Constant.APP_ID,Constant.APP_PID,this);
		// 禁止默认的页面统计方式，这样将不会再自动统计Activity
		/*
		 * MobclickAgent.openActivityDurationTrack(false);
		 * MobclickAgent.updateOnlineConfig( this );
		 */
		setAppId(this);
		init();

	}

	public static String getDeviceInfo(Context context) {
		try {
			org.json.JSONObject json = new org.json.JSONObject();
			android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);

			String device_id = tm.getDeviceId();

			android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context
					.getSystemService(Context.WIFI_SERVICE);

			String mac = wifi.getConnectionInfo().getMacAddress();
			json.put("mac", mac);

			if (TextUtils.isEmpty(device_id)) {
				device_id = mac;
			}

			if (TextUtils.isEmpty(device_id)) {
				device_id = android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);
			}

			json.put("device_id", device_id);

			return json.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 设置app唯一标示符
	 * @param context
	 */
	private void setAppId(Context context) {
		String uniqueID = appContext.getProperty(AppConfig.CONF_APP_UNIQUEID);
		if (StringUtils.isEmpty(uniqueID)) {

			android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);

			uniqueID = tm.getDeviceId();

			if (TextUtils.isEmpty(uniqueID)) {
				uniqueID = android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);
			}

			appContext.setProperty(AppConfig.CONF_APP_UNIQUEID, uniqueID);
		}else {
			appContext.setProperty(AppConfig.CONF_APP_UNIQUEID, uniqueID);
		}
		 
		String value = AppConnect.getInstance(this).getConfig("SHOWIMG", "0");
		Constant.SHOWIMG = value;
		/*
		if(value.equals("0")){
			Constant.tag1 = Constant.stag1;
		}*/
		
	}

	private void init() {
		// 使用SharedPreferences来记录程序的使用次数
		SharedPreferences preferences = getSharedPreferences(
				SHAREDPREFERENCES_NAME, MODE_PRIVATE);
		// 取得相应的值，如果没有该值，说明还未写入，用true作为默认值
		isFirstIn = preferences.getBoolean("isFirstIn", false);
		// 判断程序与第几次运行，如果是第一次运行则跳转到引导界面，否则跳转到主界面
		if (!isFirstIn) {
			// 使用Handler的postDelayed方法，1秒后执行跳转到MainActivity
			mHandler.sendEmptyMessageDelayed(GO_HOME, SPLASH_DELAY_MILLIS);
		} else {
			mHandler.sendEmptyMessageDelayed(GO_GUIDE, SPLASH_DELAY_MILLIS);
		}
	}

	/**
	 * 跳转到首页
	 */
	private void goHome() {
		final View view = View.inflate(this, R.layout.app_start, null);
		setContentView(view);
		// 渐变展示启动屏
		AlphaAnimation animation = new AlphaAnimation(0.3f, 1.0f);
		animation.setDuration(2000);
		view.startAnimation(animation);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation arg0) {
				redirectTo();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});


	}

	/**
	 * 跳转到欢迎页
	 */
	private void goGuide() {
		//Intent intent = new Intent(AppStart.this, GuideActivity.class);
		//AppStart.this.startActivity(intent);
		//AppStart.this.finish();
	}

	/**
	 * 跳转到...
	 */
	private void redirectTo() {
		Intent intent = new Intent(AppStart.this, MainActivity.class);
		AppStart.this.startActivity(intent);
		finish();
	}


	public void onResume() {
		super.onResume();
	}

	public void onPause() {
		super.onPause();
	}

}
